package com.example.holamundo

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.example.holamundo.models.Dialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), DialogAction {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        defineActions()
        Log.d("CICE", BuildConfig.PROPERTIES_VAR)
    }

    override fun onResume() {
        super.onResume()
        Log.d("CICE", "Estoy en el main activity")
    }


    fun defineActions() {
        buttonName.setOnClickListener {
            /*AlertDialog.Builder(this)
                .setTitle("My alert")
                .setMessage("My alert message")
                .setCancelable(false)
                .setNegativeButton("Cancel"
                ) { dialog, which ->
                    Toast.makeText(this@MainActivity, "NEGATIVE", Toast.LENGTH_SHORT).show()}
                .setPositiveButton("Accept") { dialog, which ->
                    Toast.makeText(this@MainActivity, "POSITIVE", Toast.LENGTH_SHORT).show()}
                .show()*/


            var dialogAlert = DialogAlert.newInstance(Dialog("Title", "text", "positive"))




            dialogAlert.show(supportFragmentManager, null)

        }
    }


    private fun replaceFragment(month: String) {
        supportFragmentManager.beginTransaction()
            .add(R.id.container, FragmentThird.newInstance(month), "FragmentThird")
            .commit()
    }


    private fun switchFramgnet() {
        var fragmentApril = supportFragmentManager.findFragmentByTag("FragmentThird")
        if (fragmentApril != null && fragmentApril.isAdded)
            replaceFragment("Mayo")
        else
            replaceFragment("Abril")
    }

    override fun positiviClick(rqcode: Int) {
        Log.d("CICE", "positive click")
        when(rqcode){
            0 -> {}
            1 -> {}
        }
    }
    override fun negativeClick(rqcode: Int) {
        Log.d("CICE", "negative click")
    }

}



