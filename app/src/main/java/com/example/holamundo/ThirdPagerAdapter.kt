package com.example.holamundo

import android.content.res.TypedArray
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class ThirdPagerAdapter(fm: FragmentManager, var months: TypedArray) : FragmentStatePagerAdapter(fm) {

    override fun getCount() = months.length()

    override fun getItem(position: Int): Fragment {
        return when (position % 3) {
            0 -> FragmentThird.newInstance(months.getString(position))
            1 -> FragmentThirdComplete.newInstance(months.getString(position), position + 1)
            2 -> FragmentThirdGrid.newInstance()
            else -> FragmentThird.newInstance(months.getString(position))
        }
    }
}