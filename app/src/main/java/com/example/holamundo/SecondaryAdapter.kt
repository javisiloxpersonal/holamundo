package com.example.holamundo

import android.content.res.TypedArray
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_number_left.view.*

class SecondaryAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        const val LEFT = 2
        const val RIGHT = 3


        fun staticMethod(){
            Log.d("CICIE","Mi metodo estático")
        }
    }

    //region VARS
    lateinit var items: TypedArray
    lateinit var listener: SecondaryRecyclerActions
    //endregion

    //region CONSTRUCTOR
    constructor(listener: SecondaryActivity) : this() {
        items = listener.resources.obtainTypedArray(R.array.secondaryItems)
        this.listener = listener
    }
    //endregion

    //region ADAPTER METHODS
    override fun getItemCount() = items.length()


    override fun getItemViewType(position: Int) =
            when {
                position % 2 > 0 -> LEFT
                else -> RIGHT
            }


    override fun onCreateViewHolder(root: ViewGroup, itemViewType: Int): RecyclerView.ViewHolder {
        val monthView: View = when (itemViewType) {
            LEFT -> LayoutInflater.from(root.context).inflate(R.layout.item_number_left, root, false)
            RIGHT -> LayoutInflater.from(root.context).inflate(R.layout.item_number_right, root, false)
            else -> LayoutInflater.from(root.context).inflate(R.layout.item_number_left, root, false)
        }

        return MonthsLeftHolder(monthView)
    }

    override fun onBindViewHolder(monthsHolder: RecyclerView.ViewHolder, itemCount: Int) {
        (monthsHolder as MonthsLeftHolder).bind(itemCount + 1, items.getString(itemCount))
    }
    //endregion

    //region HOLDER
    inner class MonthsLeftHolder(private var monthsView: View) : RecyclerView.ViewHolder(monthsView) {

        fun bind(number: Int, month: String) {
            when {
                number % 2 > 0 ->
                    monthsView.constraintItem.setBackgroundColor(ContextCompat.getColor(monthsView.context, R.color.colorPrimary))
                else ->
                    monthsView.constraintItem.setBackgroundColor(ContextCompat.getColor(monthsView.context, R.color.colorAccent))
            }
            monthsView.textNumber.text = number.toString()
            monthsView.textText.text = month
            monthsView.constraintItem.setOnClickListener { listener.onItemSelected(month)}
        }
    }
    //endregion
}