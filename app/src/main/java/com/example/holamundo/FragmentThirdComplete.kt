package com.example.holamundo

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_third.view.*

class FragmentThirdComplete : Fragment() {

    companion object {
        const val MONTH = "month"
        const val NUMBER = "number"

        fun newInstance(text: String, number: Int): FragmentThirdComplete {
            val arguments = Bundle()
            arguments.putString(MONTH, text)
            arguments.putInt(NUMBER, number)
            val fragment = FragmentThirdComplete()
            fragment.arguments = arguments
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_third, container, false)
        view.textMonth.text = arguments?.getString(FragmentThird.MONTH) ?: "UN MES CUALQUIERA"
        view.textNumber.text = arguments?.getInt(NUMBER).toString()
        view.textNumber.visibility = View.VISIBLE
        return view
    }



}