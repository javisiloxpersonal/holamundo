package com.example.holamundo

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_secondary.*

class SecondaryActivity : AppCompatActivity(), SecondaryRecyclerActions {
    companion object {
        const val MONTHS = "MONTHS"
        const val USER = "USER"
    }

    lateinit var preferences : SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_secondary)
        preferences = getSharedPreferences(USER, Context.MODE_PRIVATE)

        Picasso.get()
            .load("https://padresenlanube.com/wp-content/uploads/2015/08/balon-de-futbol.jpg")
            .centerInside()
            .resize(200, 200)
            .into(imageAvatar)
        readIntent()
    }

    private fun readIntent() {
        var name = intent.getStringExtra("name")
        showName(name)
        getMonths()
    }


    private fun showName(name: String) {
        textName.text = name
        recyclerMonths.adapter = SecondaryAdapter(this@SecondaryActivity)
        buttonGotIt.setOnClickListener {
            startActivity(Intent(this@SecondaryActivity, ThirdActivity::class.java))
        }
    }

    override fun onItemSelected(monthName: String) {
        super.onItemSelected(monthName)
        saveMonth(monthName)

    }


    private fun saveMonth(month: String) {
        val editor = preferences.edit()
        var months = getMonths() ?: mutableSetOf()

        months.add(month)
        editor.clear()
        editor.putStringSet(MONTHS, months)
        editor.apply()
    }

    private fun getMonths() : MutableSet<String>? {
        val setMonths = preferences.getStringSet(MONTHS, null)
        setMonths?.forEach { Log.d("CICE", it) }
        return setMonths
    }

}