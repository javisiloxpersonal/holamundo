package com.example.holamundo

import android.content.Context
import android.content.res.TypedArray
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_week_day.view.*

class FragmentThidGridAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var items: TypedArray

    constructor(context: Context) : this() {
        items = context.resources.obtainTypedArray(R.array.weekDaysItems)
    }

    override fun getItemCount() = items.length()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        var view = LayoutInflater.from(p0.context).inflate(R.layout.item_week_day, p0, false)
        return WeekHolder(view)
    }

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        (p0 as WeekHolder).bind(items.getString(p1))
    }

    inner class WeekHolder(var weekView: View) : RecyclerView.ViewHolder(weekView) {
        fun bind(day: String) {
            weekView.textDay.text = day

        }
    }

}