package com.example.holamundo.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class Dialog(
    var title: String? = null,
    var text: String? = null,
    var buttonPositive: String? = null,
    var buttonNegative: String? = null,
    var requestCode: Int = 0
) : Parcelable