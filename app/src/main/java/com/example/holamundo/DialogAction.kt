package com.example.holamundo

interface DialogAction {

    fun positiviClick(requestCode : Int)

    fun negativeClick(requestCode : Int)
}